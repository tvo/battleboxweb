module View.LobbyCreate exposing (view)

import Dict
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Model.Model exposing (..)
import Routing exposing (..)
import Util exposing (doesDictEntryEqual, getMaybeString)


view : Model -> Html Msg
view model =
    div
        [ classList
            [ ( "lobby-popup standard-border"
              , not <| isLobbyCreateHidden model.route
              )
            , ( "lobby-popup-hidden", isLobbyCreateHidden model.route )
            ]
        ]
        (if isLobbyCreateHidden model.route then
            []

         else
            let
                modelLobby =
                    model.lobby

                isGameType2p =
                    doesDictEntryEqual modelLobby.newLobbyForm "gameType" "0"

                isTeamA =
                    doesDictEntryEqual modelLobby.newLobbyForm "team" "0"
            in
            [ div [ class "lobby-create flex column align-center justify-center" ]
                [ div
                    [ class
                        "form-header standard-header flex row align-center justify-center"
                    ]
                    [ text "Create Lobby" ]
                , input
                    [ class "form-row"
                    , placeholder "Lobby Name"
                    , onInput <| NewLobbyUpdate "name"
                    , value <|
                        getMaybeString (Dict.get "name" modelLobby.newLobbyForm) ""
                    ]
                    []
                , div
                    [ class "form-row flex row align-center justify-center" ]
                    [ button
                        [ classList
                            [ ( "option-button", True )
                            , ( "selected", isGameType2p )
                            ]
                        , disabled isGameType2p
                        , onClick <| NewLobbyUpdate "gameType" "0"
                        ]
                        [ text "2-player" ]
                    , button
                        [ classList
                            [ ( "option-button", True )
                            , ( "selected", not isGameType2p )
                            ]
                        , disabled <| not isGameType2p
                        , onClick <| NewLobbyUpdate "gameType" "1"
                        ]
                        [ text "4-player" ]
                    ]
                , div
                    [ class "form-row flex row align-center justify-center" ]
                    [ button
                        [ classList
                            [ ( "option-button", True )
                            , ( "selected", isTeamA )
                            ]
                        , disabled isTeamA
                        , onClick <| NewLobbyUpdate "team" "0"
                        ]
                        [ text "Wolf" ]
                    , button
                        [ classList
                            [ ( "option-button", True )
                            , ( "selected", not isTeamA )
                            ]
                        , disabled <| not isTeamA
                        , onClick <| NewLobbyUpdate "team" "1"
                        ]
                        [ text "Crow" ]
                    ]
                , div
                    [ class "form-footer flex row align-center justify-center" ]
                    [ button
                        [ class "option-button cancel-button"
                        , onClick (NavPush "/lobby")
                        ]
                        [ text "Cancel" ]
                    , button
                        [ class "option-button submit-button"
                        , onClick CreateLobby
                        , disabled <| isSubmitDisabled modelLobby.newLobbyForm
                        ]
                        [ text "Create" ]
                    ]
                ]
            ]
        )


isLobbyCreateHidden : Maybe Route -> Bool
isLobbyCreateHidden route =
    case route of
        Just LobbyCreate ->
            False

        _ ->
            True


isSubmitDisabled : Dict.Dict String String -> Bool
isSubmitDisabled form =
    case Dict.get "name" form of
        Nothing ->
            True

        Just "" ->
            True

        _ ->
            False
