module View.Game exposing (view)

import Html
import Html.Attributes as HtmlAtt
import Model.Game exposing (..)
import Model.Model exposing (..)
import Routing exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Svg.Events exposing (..)
import View.Svg.Board exposing (board)
import View.Svg.Draft exposing (draft)


view : Model -> Html.Html Msg
view model =
    let
        isGameRoute =
            case model.route of
                Just (CurrentGame _) ->
                    True

                _ ->
                    False
    in
    Html.div
        [ HtmlAtt.classList
            [ ( "game standard-border", isGameRoute )
            , ( "game-hidden", not isGameRoute )
            ]
        ]
        (case model.player of
            Nothing ->
                []

            Just player ->
                case model.route of
                    Just (CurrentGame gameId) ->
                        case model.currentGame of
                            Nothing ->
                                [ Html.div [ HtmlAtt.class "error" ]
                                    [ Html.text
                                        ("No Game Found for Id: " ++ String.fromInt gameId)
                                    ]
                                ]

                            Just game ->
                                let
                                    gameState =
                                        game.state
                                in
                                case gameState.gameMode of
                                    Draft ->
                                        draft model player game

                                    Battle ->
                                        board model player game

                    _ ->
                        []
        )
