module View.Svg.Hex exposing (hex)

import Model.Game exposing (Zone)
import Model.Model exposing (..)
import Svg
import Svg.Attributes exposing (..)
import Svg.Events exposing (..)


hex : Float -> Float -> Float -> String -> Maybe Zone -> List (Svg.Svg Msg)
hex xCoord yCoord radius hexClass zone =
    let
        height =
            radius * 2.0

        edge =
            height * 0.57735

        xRight =
            String.fromFloat (xCoord + (edge / 2.0))

        xFarRight =
            String.fromFloat (xCoord + edge)

        xLeft =
            String.fromFloat (xCoord - (edge / 2.0))

        xFarLeft =
            String.fromFloat (xCoord - edge)

        yTop =
            String.fromFloat (yCoord - radius)

        yCenter =
            String.fromFloat yCoord

        yBottom =
            String.fromFloat (yCoord + radius)

        hexId =
            xLeft ++ "|" ++ yTop

        pathString =
            "M "
                ++ xLeft
                ++ " "
                ++ yTop
                ++ " H "
                ++ xRight
                ++ " L "
                ++ xFarRight
                ++ " "
                ++ yCenter
                ++ " L "
                ++ xRight
                ++ " "
                ++ yBottom
                ++ " H "
                ++ xLeft
                ++ " L "
                ++ xFarLeft
                ++ " "
                ++ yCenter
                ++ " Z"
    in
    case zone of
        Nothing ->
            [ Svg.clipPath
                [ id (hexId ++ "|hex") ]
                [ Svg.path [ d pathString ] [] ]
            , Svg.path [ id (hexId ++ "|clip"), d pathString ] []
            , Svg.use
                [ clipPath ("url(#" ++ hexId ++ "|hex)")
                , xlinkHref ("#" ++ hexId ++ "|clip")
                , class hexClass
                ]
                []
            ]

        Just z ->
            [ Svg.path
                [ class hexClass
                , onMouseOver <| ZoneMouseEnter z
                , onMouseOut ZoneMouseExit
                , d pathString
                ]
                []
            ]
