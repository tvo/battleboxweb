module View.Svg.CoinStack exposing (stack)

import Model.Game exposing (CoinLocation)
import Model.Model exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Svg.Events exposing (..)


stack : CoinLocation -> Float -> Float -> Float -> Msg -> List (Svg Msg)
stack coinLoc xCoord yCoord radius mouseDownMsg =
    let
        coin =
            [ circle
                [ cx <| String.fromFloat xCoord
                , cy <| String.fromFloat yCoord
                , r <| String.fromFloat radius
                , class (getCoinClass coinLoc mouseDownMsg)
                , onMouseDown mouseDownMsg
                ]
                []
            , getCoinLabel coinLoc xCoord yCoord mouseDownMsg
            ]

        coinWithCount =
            if coinLoc.quantity > 1 then
                let
                    offset =
                        (radius / 3) * 2

                    countX =
                        xCoord + offset

                    countY =
                        yCoord + offset
                in
                List.append
                    coin
                    [ circle
                        [ cx <| String.fromFloat countX
                        , cy <| String.fromFloat (countY - (radius / 32))
                        , r <| String.fromFloat (radius / 4)
                        , class "coin-count-back"
                        ]
                        []
                    , text_
                        [ x <| String.fromFloat countX
                        , y <| String.fromFloat countY
                        , dominantBaseline "middle"
                        , textAnchor "middle"
                        , class "svg-text coin-count-text"
                        ]
                        [ text <| String.fromInt coinLoc.quantity ]
                    ]

            else
                coin
    in
    coinWithCount


getCoinClass : CoinLocation -> Msg -> String
getCoinClass coinLoc mouseDownMsg =
    let
        class =
            "coin"
                ++ (if coinLoc.coinType > 0 then
                        if mouseDownMsg == NoOp then
                            " grabbing"

                        else
                            " grabbable"

                    else
                        ""
                   )
    in
    class


getCoinLabel : CoinLocation -> Float -> Float -> Msg -> Svg Msg
getCoinLabel coinLoc xCoord yCoord mouseDownMsg =
    let
        labelClass =
            "svg-text coin-label"
                ++ (if coinLoc.coinType > 0 then
                        if mouseDownMsg == NoOp then
                            " grabbing"

                        else
                            " grabbable"

                    else
                        ""
                   )

        label =
            case coinLoc.coinType of
                0 ->
                    "Face"

                1 ->
                    "Archer"

                2 ->
                    "Bannerman"

                3 ->
                    "Berserker"

                4 ->
                    "Bishop"

                5 ->
                    "Cavalry"

                6 ->
                    "Crossbow-"

                7 ->
                    "Earl"

                8 ->
                    "Ensign"

                9 ->
                    "Footman"

                10 ->
                    "Herald"

                11 ->
                    "Knight"

                12 ->
                    "Lancer"

                13 ->
                    "Light"

                14 ->
                    "Marshall"

                15 ->
                    "Mercenary"

                16 ->
                    "Pikeman"

                17 ->
                    "Royal"

                18 ->
                    "Scout"

                19 ->
                    "Swordsman"

                20 ->
                    "Warrior"

                _ ->
                    "???"

        label2 =
            case coinLoc.coinType of
                0 ->
                    Just "Down"

                6 ->
                    Just "man"

                13 ->
                    Just "Cavalry"

                17 ->
                    Just "Guard"

                20 ->
                    Just "Priest"

                _ ->
                    Nothing
    in
    text_
        [ x <| String.fromFloat xCoord
        , y <| String.fromFloat yCoord
        , dominantBaseline "middle"
        , textAnchor "middle"
        , class labelClass
        , onMouseDown mouseDownMsg
        ]
        (case label2 of
            Nothing ->
                [ text label ]

            Just l2 ->
                [ tspan [ x <| String.fromFloat xCoord, dy "-0.6em" ] [ text label ]
                , tspan [ x <| String.fromFloat xCoord, dy "1.2em" ] [ text l2 ]
                ]
        )
