module View.Svg.PlayerZones exposing (playerDropZones, playerZones)

import Model.Common exposing (Player, SvgCoord)
import Model.Game exposing (..)
import Model.Model exposing (..)
import Svg
import Svg.Attributes exposing (..)
import Svg.Events exposing (..)
import View.Svg.CoinStack exposing (stack)
import View.Svg.HexGrid exposing (getCoinRadius)


playerZones : Player -> Game -> Float -> Float -> List (Svg.Svg Msg)
playerZones player game boardWidth boardHeight =
    drawPlayerZones player game boardWidth boardHeight


playerDropZones : Player -> Float -> Float -> List (Svg.Svg Msg)
playerDropZones player boardWidth boardHeight =
    let
        playerDropZoneBox =
            drawDropZoneBox 0 2 boardWidth boardHeight
    in
    [ playerDropZoneBox (Hand player.id)
    , playerDropZoneBox (DiscardUp player.id)
    , playerDropZoneBox (DiscardDown player.id)
    , playerDropZoneBox (Exile player.id)
    ]


drawPlayerZones : Player -> Game -> Float -> Float -> List (Svg.Svg Msg)
drawPlayerZones player game boardWidth boardHeight =
    let
        hoverZone =
            game.hoverZone

        coinRadius =
            getCoinRadius boardHeight

        gameCoins =
            game.state |> .coins

        playerStartCoords =
            getStartCoordByZone 0 2 boardWidth boardHeight

        playerZoneBox =
            drawZoneBox 0 2 boardWidth boardHeight

        handStartCoord =
            playerStartCoords (Hand player.id)

        discardUpStartCoord =
            playerStartCoords (DiscardUp player.id)

        discardDownStartCoord =
            playerStartCoords (DiscardDown player.id)

        exileStartCoord =
            playerStartCoords (Exile player.id)

        handCoinLocs =
            getCoinsByZone gameCoins (Hand player.id)

        discardUpCoinLocs =
            getCoinsByZone gameCoins (DiscardUp player.id)

        discardDownCoinLocs =
            getCoinsByZone gameCoins (DiscardDown player.id)

        exileCoinLocs =
            getCoinsByZone gameCoins (Exile player.id)
    in
    Svg.defs
        []
        [ Svg.linearGradient
            [ id "gradient-left", x1 "100%", y1 "0%", x2 "0%", y2 "0%" ]
            [ Svg.stop [ offset "0%", style "stop-color:navy;stop-opacity:1" ] []
            , Svg.stop [ offset "100%", style "stop-color:navy;stop-opacity:0" ] []
            ]
        , Svg.linearGradient
            [ id "gradient-right", x1 "0%", y1 "0%", x2 "100%", y2 "0%" ]
            [ Svg.stop [ offset "0%", style "stop-color:navy;stop-opacity:1" ] []
            , Svg.stop [ offset "100%", style "stop-color:navy;stop-opacity:0" ] []
            ]
        , Svg.linearGradient
            [ id "gradient-up", x1 "0%", y1 "100%", x2 "0%", y2 "0%" ]
            [ Svg.stop [ offset "0%", style "stop-color:navy;stop-opacity:1" ] []
            , Svg.stop [ offset "100%", style "stop-color:navy;stop-opacity:0" ] []
            ]
        , Svg.linearGradient
            [ id "gradient-down", x1 "0%", y1 "0%", x2 "0%", y2 "100%" ]
            [ Svg.stop [ offset "0%", style "stop-color:navy;stop-opacity:1" ] []
            , Svg.stop [ offset "100%", style "stop-color:navy;stop-opacity:0" ] []
            ]
        ]
        :: playerZoneBox (Bag player.id) hoverZone
        ++ playerZoneBox (Hand player.id) hoverZone
        ++ playerZoneBox (DiscardUp player.id) hoverZone
        ++ playerZoneBox (DiscardDown player.id) hoverZone
        ++ playerZoneBox (Exile player.id) hoverZone
        ++ drawZoneCoins
            handCoinLocs
            (Tuple.first handStartCoord)
            (Tuple.second handStartCoord)
            coinRadius
            0.0
            True
        ++ drawZoneCoins
            discardUpCoinLocs
            (Tuple.first discardUpStartCoord)
            (Tuple.second discardUpStartCoord)
            coinRadius
            0.0
            True
        ++ drawZoneCoins
            discardDownCoinLocs
            (Tuple.first discardDownStartCoord)
            (Tuple.second discardDownStartCoord)
            coinRadius
            0.0
            True
        ++ drawZoneCoins
            exileCoinLocs
            (Tuple.first exileStartCoord)
            (Tuple.second exileStartCoord)
            coinRadius
            0.0
            True


getStartCoordByZone : Int -> Int -> Float -> Float -> Zone -> ( Float, Float )
getStartCoordByZone playerIndex playerCount boardWidth boardHeight zone =
    let
        coinRadius =
            getCoinRadius boardHeight

        zoneSize =
            coinRadius * 2.5

        isHorizontal =
            if playerCount > 2 then
                modBy 2 playerIndex == 0

            else
                True

        xCoord =
            if isHorizontal then
                case zone of
                    Hand _ ->
                        (boardWidth / 2.0) - zoneSize

                    DiscardUp _ ->
                        (boardWidth / 2.0) + (zoneSize * 2.0) + 5.0

                    DiscardDown _ ->
                        (boardWidth / 2.0) + (zoneSize * 2.0) + 5.0

                    Bag _ ->
                        (boardWidth / 2.0) - (zoneSize * 2.0) - 10.0

                    Exile _ ->
                        (boardWidth / 2.0) - (zoneSize * 2.0) - 10.0

                    _ ->
                        0.0

            else if playerIndex == 1 then
                zoneSize / 2.0

            else
                boardWidth - (zoneSize / 2.0)

        yCoord =
            if isHorizontal then
                if playerIndex == 0 then
                    case zone of
                        DiscardDown _ ->
                            (boardHeight * (1.0 - (0.15 / 2.0))) - zoneSize - 5.0

                        Bag _ ->
                            (boardHeight * (1.0 - (0.15 / 2.0))) - zoneSize - 5.0

                        _ ->
                            boardHeight * (1.0 - (0.15 / 2.0))

                else
                    case zone of
                        DiscardDown _ ->
                            (boardHeight * (0.15 / 2.0)) + zoneSize + 5.0

                        Bag _ ->
                            (boardHeight * (0.15 / 2.0)) + zoneSize + 5.0

                        _ ->
                            boardHeight * (0.15 / 2.0)

            else
                0.0
    in
    ( xCoord, yCoord )


drawZoneBox : Int -> Int -> Float -> Float -> Zone -> Zone -> List (Svg.Svg Msg)
drawZoneBox playerIndex playerCount boardWidth boardHeight zone hoverZone =
    let
        startCoord =
            getStartCoordByZone playerIndex playerCount boardWidth boardHeight zone

        isHoverZone =
            zone == hoverZone

        zoneClass =
            if isHoverZone then
                "player-zone hover"

            else
                "player-zone"

        coinRadius =
            getCoinRadius boardHeight

        zoneSize =
            coinRadius * 2.5

        halfZoneSize =
            zoneSize / 2.0

        isHorizontal =
            if playerCount > 2 then
                modBy 2 playerIndex == 0

            else
                True

        isHand =
            case zone of
                Hand _ ->
                    True

                _ ->
                    False

        isFlipped =
            case zone of
                Bag _ ->
                    True

                Exile _ ->
                    True

                _ ->
                    False

        xCoord =
            if isHorizontal && isFlipped then
                Tuple.first startCoord + halfZoneSize

            else
                Tuple.first startCoord - halfZoneSize

        yCoord =
            if isHorizontal || not isFlipped then
                Tuple.second startCoord - halfZoneSize

            else
                Tuple.second startCoord + halfZoneSize

        zoneHeight =
            if isHorizontal then
                zoneSize

            else if isHand then
                zoneSize * 3.0

            else
                5.0

        zoneWidth =
            if isHorizontal then
                if isHand then
                    zoneSize * 3.0

                else
                    5.0

            else
                zoneSize

        zoneBox =
            if isHand then
                [ Svg.rect
                    [ x <| String.fromFloat xCoord
                    , y <| String.fromFloat yCoord
                    , width <| String.fromFloat zoneWidth
                    , height <| String.fromFloat zoneHeight
                    , class zoneClass
                    ]
                    []
                ]

            else if isFlipped then
                let
                    pathString =
                        if isHorizontal then
                            "M "
                                ++ String.fromFloat xCoord
                                ++ " "
                                ++ String.fromFloat yCoord
                                ++ " H "
                                ++ String.fromFloat (xCoord + zoneWidth)
                                ++ " V "
                                ++ String.fromFloat (yCoord + zoneHeight)
                                ++ " H "
                                ++ String.fromFloat xCoord

                        else
                            "M "
                                ++ String.fromFloat xCoord
                                ++ " "
                                ++ String.fromFloat yCoord
                                ++ " V "
                                ++ String.fromFloat (yCoord + zoneHeight)
                                ++ " H "
                                ++ String.fromFloat (xCoord + zoneWidth)
                                ++ " V "
                                ++ String.fromFloat yCoord

                    hoverBox =
                        if isHorizontal then
                            Svg.rect
                                [ x <| String.fromFloat (xCoord - (zoneSize * 5.0))
                                , y <| String.fromFloat yCoord
                                , width <| String.fromFloat (zoneSize * 5.0)
                                , height <| String.fromFloat zoneHeight
                                , fill "url(#gradient-left)"
                                , fillOpacity
                                    (if isHoverZone then
                                        "1"

                                     else
                                        "0"
                                    )
                                ]
                                []

                        else
                            Svg.rect
                                [ x <| String.fromFloat xCoord
                                , y <| String.fromFloat (yCoord - (zoneSize * 5.0))
                                , width <| String.fromFloat zoneWidth
                                , height <| String.fromFloat (zoneSize * 5.0)
                                , fill "url(#gradient-up)"
                                , fillOpacity
                                    (if isHoverZone then
                                        "1"

                                     else
                                        "0"
                                    )
                                ]
                                []
                in
                [ Svg.path [ d pathString, class zoneClass ] []
                , hoverBox
                ]

            else
                let
                    pathString =
                        if isHorizontal then
                            "M "
                                ++ String.fromFloat (xCoord + zoneWidth)
                                ++ " "
                                ++ String.fromFloat yCoord
                                ++ " H "
                                ++ String.fromFloat xCoord
                                ++ " V "
                                ++ String.fromFloat (yCoord + zoneHeight)
                                ++ " H "
                                ++ String.fromFloat (xCoord + zoneWidth)

                        else
                            "M "
                                ++ String.fromFloat xCoord
                                ++ " "
                                ++ String.fromFloat (yCoord + zoneHeight)
                                ++ " V "
                                ++ String.fromFloat yCoord
                                ++ " H "
                                ++ String.fromFloat (xCoord + zoneWidth)
                                ++ " V "
                                ++ String.fromFloat (yCoord + zoneHeight)

                    hoverBox =
                        if isHorizontal then
                            Svg.rect
                                [ x <| String.fromFloat (xCoord + zoneWidth)
                                , y <| String.fromFloat yCoord
                                , width <| String.fromFloat (zoneSize * 5.0)
                                , height <| String.fromFloat zoneHeight
                                , fill "url(#gradient-right)"
                                , fillOpacity
                                    (if isHoverZone then
                                        "1"

                                     else
                                        "0"
                                    )
                                ]
                                []

                        else
                            Svg.rect
                                [ x <| String.fromFloat xCoord
                                , y <| String.fromFloat (yCoord + zoneHeight)
                                , width <| String.fromFloat zoneWidth
                                , height <| String.fromFloat (zoneSize * 5.0)
                                , fill "url(#gradient-down)"
                                , fillOpacity
                                    (if isHoverZone then
                                        "1"

                                     else
                                        "0"
                                    )
                                ]
                                []
                in
                [ Svg.path [ d pathString, class zoneClass ] []
                , hoverBox
                ]

        zoneLabelText =
            getZoneLabelText zone

        zoneLabel =
            if isHorizontal then
                case zone of
                    DiscardDown _ ->
                        if playerIndex == 0 then
                            createZoneLabel
                                (xCoord + 5.0)
                                (yCoord - 5.0)
                                False
                                False
                                zoneLabelText

                        else
                            createZoneLabel
                                (xCoord + 5.0)
                                (yCoord + zoneSize + 5.0)
                                True
                                False
                                zoneLabelText

                    DiscardUp _ ->
                        if playerIndex == 0 then
                            createZoneLabel
                                (xCoord + 5.0)
                                (yCoord + zoneSize + 5.0)
                                True
                                False
                                zoneLabelText

                        else
                            createZoneLabel
                                (xCoord + 5.0)
                                (yCoord - 5.0)
                                False
                                False
                                zoneLabelText

                    Bag _ ->
                        if playerIndex == 0 then
                            createZoneLabel
                                xCoord
                                (yCoord - 5.0)
                                False
                                True
                                zoneLabelText

                        else
                            createZoneLabel
                                xCoord
                                (yCoord + 5.0)
                                True
                                True
                                zoneLabelText

                    Exile _ ->
                        if playerIndex == 0 then
                            createZoneLabel
                                xCoord
                                (yCoord + zoneSize + 5.0)
                                True
                                True
                                zoneLabelText

                        else
                            createZoneLabel
                                xCoord
                                (yCoord - 5.0)
                                False
                                True
                                zoneLabelText

                    _ ->
                        createZoneLabel
                            (xCoord + 5.0)
                            (yCoord - 5.0)
                            False
                            False
                            zoneLabelText

            else
                Svg.text ""
    in
    zoneBox ++ [ zoneLabel ]


drawDropZoneBox : Int -> Int -> Float -> Float -> Zone -> Svg.Svg Msg
drawDropZoneBox playerIndex playerCount boardWidth boardHeight zone =
    let
        startCoord =
            getStartCoordByZone playerIndex playerCount boardWidth boardHeight zone

        coinRadius =
            getCoinRadius boardHeight

        zoneSize =
            coinRadius * 2.5

        halfZoneSize =
            zoneSize / 2.0

        isHorizontal =
            if playerCount > 2 then
                modBy 2 playerIndex == 0

            else
                True

        isHand =
            case zone of
                Hand _ ->
                    True

                _ ->
                    False

        isFlipped =
            case zone of
                Bag _ ->
                    True

                Exile _ ->
                    True

                _ ->
                    False

        xCoord =
            if isHorizontal && isFlipped then
                Tuple.first startCoord - (zoneSize * 4.5)

            else
                Tuple.first startCoord - halfZoneSize

        yCoord =
            if isHorizontal || not isFlipped then
                Tuple.second startCoord - halfZoneSize

            else
                Tuple.second startCoord + (zoneSize * 4.5)

        zoneHeight =
            if isHorizontal then
                zoneSize

            else if isHand then
                zoneSize * 3.0

            else
                (zoneSize * 5.0) + 5.0

        zoneWidth =
            if isHorizontal then
                if isHand then
                    zoneSize * 3.0

                else
                    (zoneSize * 5.0) + 5.0

            else
                zoneSize
    in
    Svg.rect
        [ x <| String.fromFloat xCoord
        , y <| String.fromFloat yCoord
        , width <| String.fromFloat zoneWidth
        , height <| String.fromFloat zoneHeight
        , class "drop-zone"
        , onMouseOver <| ZoneMouseEnter zone
        , onMouseOut ZoneMouseExit
        ]
        []


drawZoneCoins : List CoinLocation -> Float -> Float -> Float -> Float -> Bool -> List (Svg.Svg Msg)
drawZoneCoins zoneCoinLocs startX startY coinRadius index isHorizontal =
    let
        head =
            List.head zoneCoinLocs

        tail =
            List.tail zoneCoinLocs
    in
    case head of
        Nothing ->
            []

        Just h ->
            let
                isFlipped =
                    case h.zone of
                        Bag _ ->
                            True

                        Exile _ ->
                            True

                        _ ->
                            False

                xCoord =
                    getZoneOffset startX coinRadius index isHorizontal isFlipped

                yCoord =
                    getZoneOffset startY coinRadius index (not isHorizontal) isFlipped

                headCoin =
                    stack h xCoord yCoord coinRadius (CoinMouseDown h <| SvgCoord xCoord yCoord)
            in
            case tail of
                Nothing ->
                    headCoin

                Just [] ->
                    headCoin

                Just moreCoins ->
                    headCoin
                        ++ drawZoneCoins
                            moreCoins
                            startX
                            startY
                            coinRadius
                            (index + 1.0)
                            isHorizontal


getCoinsByZone : List CoinLocation -> Zone -> List CoinLocation
getCoinsByZone coins zone =
    List.filter
        (\cl -> cl.zone == zone)
        coins


getZoneOffset : Float -> Float -> Float -> Bool -> Bool -> Float
getZoneOffset start coinRadius index isOffsetAxis isFlipped =
    let
        offset =
            coinRadius * 2.5
    in
    if isOffsetAxis then
        if isFlipped then
            start - (offset * index)

        else
            start + (offset * index)

    else
        start


createZoneLabel : Float -> Float -> Bool -> Bool -> String -> Svg.Svg Msg
createZoneLabel xCoord yCoord isHanging isRightAligned zoneText =
    Svg.text_
        [ x <| String.fromFloat xCoord
        , y <| String.fromFloat yCoord
        , class "svg-text zone-label"
        , dominantBaseline
            (if isHanging then
                "hanging"

             else
                "baseline"
            )
        , textAnchor
            (if isRightAligned then
                "end"

             else
                "start"
            )
        ]
        [ Svg.text zoneText ]


getZoneLabelText : Zone -> String
getZoneLabelText zone =
    case zone of
        Bag _ ->
            "Your Bag"

        Hand _ ->
            "Your Hand"

        DiscardUp _ ->
            "Your Discard Face-Up"

        DiscardDown _ ->
            "Your Discard Face-Down"

        Exile _ ->
            "Your Exile"

        _ ->
            "???"
