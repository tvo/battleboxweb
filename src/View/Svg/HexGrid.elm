module View.Svg.HexGrid exposing (getCoinRadius, hexDropZones, hexGrid)

import Model.Common exposing (SvgCoord)
import Model.Game exposing (..)
import Model.Model exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Svg.Events exposing (..)
import View.Svg.CoinStack exposing (stack)
import View.Svg.Hex exposing (hex)


hexGrid : Game -> Float -> Float -> List (Svg Msg)
hexGrid game boardWidth boardHeight =
    let
        gameState =
            game.state

        hexHeight =
            boardHeight / 10.0

        centerX =
            boardWidth / 2.0

        centerY =
            boardHeight / 2.0

        boardCoords =
            game.boardCoords

        controlCoords =
            game.controlCoords
    in
    drawHexes game hexHeight centerX centerY "hex-space" boardCoords
        ++ drawHexes game hexHeight centerX centerY "hex-space control" controlCoords
        ++ drawCoins game hexHeight centerX centerY gameState.coins


hexDropZones : Game -> Float -> Float -> List (Svg Msg)
hexDropZones game boardWidth boardHeight =
    let
        hexHeight =
            boardHeight / 10.0

        centerX =
            boardWidth / 2.0

        centerY =
            boardHeight / 2.0

        boardCoords =
            game.boardCoords

        controlCoords =
            game.controlCoords
    in
    drawHexes game hexHeight centerX centerY "drop-zone" boardCoords
        ++ drawHexes game hexHeight centerX centerY "drop-zone" controlCoords


getCoinRadius : Float -> Float
getCoinRadius boardHeight =
    (boardHeight / 10.0) * 0.4


drawHexes : Game -> Float -> Float -> Float -> String -> List AxialCoord -> List (Svg Msg)
drawHexes game hexHeight centerX centerY hexClass axialCoords =
    let
        innerRadius =
            hexHeight / 2.0

        edge =
            hexHeight * 0.57735

        head =
            List.head axialCoords

        tail =
            List.tail axialCoords
    in
    case head of
        Nothing ->
            []

        Just h ->
            let
                isDropZone =
                    hexClass == "drop-zone"

                qFloat =
                    toFloat h.q

                rFloat =
                    toFloat h.r

                xCoord =
                    centerX + (qFloat * (edge * 1.5))

                yCoord =
                    centerY + (qFloat * innerRadius) + (rFloat * (innerRadius * 2.0))

                zone =
                    if isDropZone then
                        Just <| Board h.q h.r

                    else
                        Nothing

                classWithHover =
                    if isDropZone then
                        hexClass

                    else
                        case game.hoverZone of
                            Board q r ->
                                if q == h.q && r == h.r then
                                    hexClass ++ " hover"

                                else
                                    hexClass

                            _ ->
                                hexClass

                headHex =
                    hex xCoord yCoord innerRadius classWithHover zone
            in
            case tail of
                Nothing ->
                    headHex

                Just [] ->
                    headHex

                Just t ->
                    headHex
                        ++ drawHexes game hexHeight centerX centerY hexClass t


drawCoins : Game -> Float -> Float -> Float -> List CoinLocation -> List (Svg Msg)
drawCoins game hexHeight centerX centerY coins =
    let
        head =
            List.head coins

        tail =
            List.tail coins
    in
    case head of
        Nothing ->
            []

        Just coinLoc ->
            case coinLoc.zone of
                Board q r ->
                    let
                        coinRadius =
                            hexHeight * 0.4

                        coinCoords =
                            getCoordFromAxial (AxialCoord q r) hexHeight centerX centerY

                        xCoord =
                            Tuple.first coinCoords

                        yCoord =
                            Tuple.second coinCoords

                        coinStack =
                            stack
                                coinLoc
                                xCoord
                                yCoord
                                coinRadius
                                (CoinMouseDown coinLoc <| SvgCoord xCoord yCoord)
                    in
                    case tail of
                        Nothing ->
                            coinStack

                        Just [] ->
                            coinStack

                        Just moreCoins ->
                            coinStack
                                ++ drawCoins game hexHeight centerX centerY moreCoins

                _ ->
                    case tail of
                        Nothing ->
                            []

                        Just [] ->
                            []

                        Just moreCoins ->
                            drawCoins game hexHeight centerX centerY moreCoins


getCoordFromAxial : AxialCoord -> Float -> Float -> Float -> ( Float, Float )
getCoordFromAxial axialCoord hexHeight centerX centerY =
    let
        hexRadius =
            hexHeight / 2.0

        edge =
            hexHeight * 0.57735

        qFloat =
            toFloat axialCoord.q

        rFloat =
            toFloat axialCoord.r

        xCoord =
            centerX + (qFloat * (edge * 1.5))

        yCoord =
            centerY + (qFloat * hexRadius) + (rFloat * (hexRadius * 2.0))
    in
    ( xCoord, yCoord )
