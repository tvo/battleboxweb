module View.Svg.Board exposing (board)

import Json.Decode as JsonD
import Model.Common exposing (Player, SvgCoord)
import Model.Game exposing (..)
import Model.Model exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Svg.Events exposing (..)
import View.Svg.CoinStack exposing (stack)
import View.Svg.HexGrid exposing (getCoinRadius, hexDropZones, hexGrid)
import View.Svg.PlayerZones exposing (playerDropZones, playerZones)


board : Model -> Player -> Game -> List (Svg Msg)
board model player game =
    let
        screenWidth =
            model.screenSize |> .width |> toFloat

        boardWidth =
            screenWidth * 0.93

        screenHeight =
            model.screenSize |> .height |> toFloat

        boardHeight =
            screenHeight * 0.95

        coinRadius =
            getCoinRadius boardHeight
    in
    [ svg
        [ on "mousemove" (JsonD.map MouseMove mouseMoveDecoder)
        , onMouseUp CoinMouseUp
        , width (String.fromFloat boardWidth)
        , height (String.fromFloat boardHeight)
        , viewBox
            ("0 0 "
                ++ String.fromFloat boardWidth
                ++ " "
                ++ String.fromFloat boardHeight
            )
        ]
        (hexGrid game boardWidth boardHeight
            ++ playerZones player game boardWidth boardHeight
            ++ addDragCoin game.coinDrag game.mouseCoord coinRadius
            ++ addDropZones player game boardWidth boardHeight
        )
    ]


mouseMoveDecoder : JsonD.Decoder SvgCoord
mouseMoveDecoder =
    JsonD.map2 SvgCoord
        (JsonD.at [ "x" ] JsonD.float)
        (JsonD.at [ "y" ] JsonD.float)


addDragCoin : Maybe CoinDrag -> SvgCoord -> Float -> List (Svg Msg)
addDragCoin coinDrag mouseCoord coinRadius =
    case coinDrag of
        Nothing ->
            []

        Just cd ->
            let
                coinLoc =
                    cd.coinLoc

                startCoinCoord =
                    cd.startCoinCoord

                startMouseCoord =
                    cd.startMouseCoord

                offsetX =
                    mouseCoord.x - startMouseCoord.x

                offsetY =
                    mouseCoord.y - startMouseCoord.y
            in
            stack
                coinLoc
                (startCoinCoord.x + offsetX)
                (startCoinCoord.y + offsetY)
                coinRadius
                NoOp


addDropZones : Player -> Game -> Float -> Float -> List (Svg Msg)
addDropZones player game boardWidth boardHeight =
    if game.coinDrag == Nothing then
        []

    else
        hexDropZones game boardWidth boardHeight
            ++ playerDropZones player boardWidth boardHeight
