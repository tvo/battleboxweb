module View.Svg.Draft exposing (draft)

import Json.Decode as JsonD
import Model.Common exposing (Player, SvgCoord)
import Model.Game exposing (..)
import Model.Model exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Svg.Events exposing (..)
import View.Svg.CoinStack exposing (stack)


draft : Model -> Player -> Game -> List (Svg Msg)
draft model player game =
    let
        screenWidth =
            model.screenSize |> .width |> toFloat

        boardWidth =
            screenWidth * 0.93

        screenHeight =
            model.screenSize |> .height |> toFloat

        boardHeight =
            screenHeight * 0.95

        gameState =
            game.state
    in
    [ svg
        [ on "mousemove" (JsonD.map MouseMove mouseMoveDecoder)
        , onMouseUp CoinMouseUp
        , width (String.fromFloat boardWidth)
        , height (String.fromFloat boardHeight)
        , viewBox
            ("0 0 "
                ++ String.fromFloat boardWidth
                ++ " "
                ++ String.fromFloat boardHeight
            )
        ]
        (List.concat
            [ [ rect
                    [ x <| String.fromFloat ((boardWidth * 0.5) - 200.0)
                    , y <| String.fromFloat ((boardHeight * 0.25) - 100.0)
                    , width "400"
                    , height "200"
                    , stroke "gray"
                    , fill
                        (if game.hoverZone == Draftable then
                            "navy"

                         else
                            "black"
                        )
                    ]
                    []
              , rect
                    [ x <| String.fromFloat ((boardWidth * 0.5) - 200.0)
                    , y <| String.fromFloat ((boardHeight * 0.75) - 100.0)
                    , width "400"
                    , height "200"
                    , stroke "gray"
                    , fill
                        (if game.hoverZone == Drafted 1 then
                            "navy"

                         else
                            "black"
                        )
                    ]
                    []
              ]
            , createCoins gameState.coins boardWidth boardHeight
            , addZones game boardWidth boardHeight
            ]
        )
    ]


mouseMoveDecoder : JsonD.Decoder SvgCoord
mouseMoveDecoder =
    JsonD.map2 SvgCoord
        (JsonD.at [ "x" ] JsonD.float)
        (JsonD.at [ "y" ] JsonD.float)


createCoins : List CoinLocation -> Float -> Float -> List (Svg Msg)
createCoins coins boardWidth boardHeight =
    let
        h =
            List.head coins

        t =
            List.tail coins
    in
    case h of
        Nothing ->
            []

        Just coinLoc ->
            let
                coinCoord =
                    getCoordFromZone
                        coinLoc.zone
                        boardWidth
                        boardHeight

                x =
                    Tuple.first coinCoord

                y =
                    Tuple.second coinCoord

                headCoin =
                    stack coinLoc x y 50.0 (CoinMouseDown coinLoc <| SvgCoord x y)
            in
            case t of
                Nothing ->
                    headCoin

                Just moreCoins ->
                    headCoin
                        ++ createCoins moreCoins boardWidth boardHeight


getCoordFromZone : Zone -> Float -> Float -> ( Float, Float )
getCoordFromZone zone boardWidth boardHeight =
    case zone of
        Draftable ->
            ( boardWidth * 0.5, boardHeight * 0.25 )

        Drafted _ ->
            ( boardWidth * 0.5, boardHeight * 0.75 )

        _ ->
            ( 0.0, 0.0 )


addZones : Game -> Float -> Float -> List (Svg Msg)
addZones game boardWidth boardHeight =
    if game.coinDrag == Nothing then
        []

    else
        [ rect
            [ x <| String.fromFloat ((boardWidth * 0.5) - 200.0)
            , y <| String.fromFloat ((boardHeight * 0.25) - 100.0)
            , class "drop-zone grabbing"
            , width "400"
            , height "200"
            , onMouseOver <| ZoneMouseEnter Draftable
            , onMouseOut ZoneMouseExit
            ]
            []
        , rect
            [ x <| String.fromFloat ((boardWidth * 0.5) - 200.0)
            , y <| String.fromFloat ((boardHeight * 0.75) - 100.0)
            , class "drop-zone grabbing"
            , width "400"
            , height "200"
            , onMouseOver <| ZoneMouseEnter (Drafted 1)
            , onMouseOut ZoneMouseExit
            ]
            []
        ]
