module View.Loading exposing (view)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Model.Model exposing (Model, Msg)


view : Model -> Browser.Document Msg
view _ =
    { title = "Loading..."
    , body =
        [ div []
            [ text "Loading..." ]
        ]
    }
