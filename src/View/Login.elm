module View.Login exposing (view)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)
import Model.Model exposing (..)
import Util exposing (getMaybeString)


view : Model -> Browser.Document Msg
view model =
    { title = "Battlebox Login"
    , body =
        [ div
            [ class
                "login-form standard-border flex column justify-center align-center"
            ]
            [ text "Please Enter Your Player ID"
            , input
                [ class "id-input"
                , placeholder "#"
                , onInput LoginIdUpdate
                , value <| getMaybeString model.playerId ""
                ]
                []
            , button [ onClick IdEntered ] [ text "Submit" ]
            ]
        ]
    }
