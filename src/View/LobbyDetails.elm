module View.LobbyDetails exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import Model.Common exposing (Player)
import Model.Lobby exposing (Lobby)
import Model.Model exposing (..)
import Routing exposing (..)


view : Model -> Html Msg
view model =
    let
        isLobbyDetailRoute =
            case model.route of
                Just (LobbyDetails _) ->
                    True

                _ ->
                    False

        modelLobby =
            model.lobby

        currentLobbies =
            case modelLobby.lobbies of
                Just lobbies ->
                    lobbies

                Nothing ->
                    []

        currentLobby =
            case model.route of
                Just (LobbyDetails lobbyId) ->
                    List.filter (\lobby -> lobby.id == lobbyId) currentLobbies
                        |> List.head

                _ ->
                    Nothing

        player =
            case model.player of
                Just p ->
                    p

                _ ->
                    Player -1 ""
    in
    div
        [ classList
            [ ( "lobby-popup standard-border", isLobbyDetailRoute )
            , ( "lobby-popup-hidden", isLobbyDetailRoute )
            ]
        ]
        (case model.route of
            Just (LobbyDetails lobbyId) ->
                case currentLobby of
                    Nothing ->
                        [ div [ class "lobby-details flex column align-center justify-center" ]
                            [ div
                                [ class
                                    "form-header standard-header flex row align-center justify-center"
                                ]
                                [ text "Missing Lobby..." ]
                            ]
                        , div [ class "error" ]
                            [ text
                                ("No Lobby Found with Id: " ++ String.fromInt lobbyId)
                            ]
                        ]

                    Just lobby ->
                        let
                            isTeamA =
                                List.any (\p -> p.id == player.id) lobby.teamA

                            isTeamB =
                                List.any (\p -> p.id == player.id) lobby.teamB
                        in
                        [ div [ class "lobby-details flex column align-center justify-center" ]
                            [ div
                                [ class
                                    "form-header standard-header flex row align-center justify-between"
                                ]
                                [ div [] []
                                , div [] [ text ("Lobby: " ++ lobby.name) ]
                                , button
                                    [ class "cancel-button close-button"
                                    , onClick (NavPush "/lobby")
                                    ]
                                    [ text "X" ]
                                ]
                            , div
                                [ class "form-row flex row align-center justify-center" ]
                                [ button
                                    [ classList
                                        [ ( "option-button", True )
                                        , ( "selected", isTeamA )
                                        ]
                                    , disabled isTeamA
                                    , onClick (JoinLobby lobby.id "0")
                                    ]
                                    [ text "Wolf" ]
                                , button
                                    [ classList
                                        [ ( "option-button", True )
                                        , ( "selected", isTeamB )
                                        ]
                                    , disabled isTeamB
                                    , onClick (JoinLobby lobby.id "1")
                                    ]
                                    [ text "Crow" ]
                                ]
                            , div [ class "form-row flex row align center justify-center" ]
                                [ addPlayerList lobby.teamA
                                , addPlayerList lobby.teamB
                                ]
                            , addLeaveOption lobby player.id isTeamA isTeamB
                            , addCreatorOptions player.id lobby
                            ]
                        ]

            _ ->
                []
        )


addLeaveOption : Lobby -> Int -> Bool -> Bool -> Html Msg
addLeaveOption lobby playerId isTeamA isTeamB =
    if isTeamA || isTeamB && lobby.createdById /= playerId then
        div
            [ class "form-row flex row align-center justify-center" ]
            [ button
                [ class "option-button cancel-button", onClick (LeaveLobby lobby.id) ]
                [ text "Leave Lobby" ]
            ]

    else
        div [] []


addCreatorOptions : Int -> Lobby -> Html Msg
addCreatorOptions playerId lobby =
    if lobby.createdById == playerId then
        let
            playerCount =
                List.length lobby.teamA + List.length lobby.teamB

            maxPlayers =
                case lobby.gameType of
                    0 ->
                        2

                    1 ->
                        4

                    _ ->
                        0

            buttons =
                if playerCount < maxPlayers then
                    [ button
                        [ class "option-button cancel-button"
                        , onClick (DeleteLobby lobby.id)
                        ]
                        [ text "Delete Lobby" ]
                    ]

                else
                    [ button
                        [ class "option-button cancel-button"
                        , onClick (DeleteLobby lobby.id)
                        ]
                        [ text "Delete Lobby" ]
                    , button
                        [ class "option-button submit-button"
                        , onClick (LobbyToGame lobby.id)
                        ]
                        [ text "Start Game" ]
                    ]
        in
        div
            [ class "form-row flex row align-center justify-center" ]
            buttons

    else
        div [] []


addPlayerList : List Player -> Html Msg
addPlayerList players =
    ul [ class "player-list" ]
        (buildPlayerList players)


buildPlayerList : List Player -> List (Html Msg)
buildPlayerList players =
    let
        head =
            List.head players

        tail =
            List.tail players
    in
    case head of
        Nothing ->
            []

        Just h ->
            let
                headEntry =
                    li [] [ text h.name ]
            in
            case tail of
                Nothing ->
                    [ headEntry ]

                Just [] ->
                    [ headEntry ]

                Just t ->
                    headEntry :: buildPlayerList t
