module View.Lobby exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import List exposing (append, head, tail)
import Model.Common exposing (GameMeta, Player)
import Model.Lobby exposing (..)
import Model.Model exposing (..)
import Routing exposing (..)


view : Model -> Html Msg
view model =
    div
        [ class
            ("lobby-section" ++ getLobbyHiddenClass model.route)
        ]
        (if isLobbyHidden model.route then
            [ button [ onClick (NavPush "/lobby") ] [ text "<<" ] ]

         else
            let
                isCreateLobby =
                    model.route == Just LobbyCreate
            in
            [ div [ class "lobby-splash flex justify-center align-center" ]
                [ text "BattleBox" ]
            , div [ class "lobby-content flex column" ]
                [ div
                    [ class
                        "lobby-header standard-header flex align-center justify-end"
                    ]
                    [ button
                        [ class "cancel-button"
                        , onClick Logout
                        ]
                        [ text "Logout" ]
                    ]
                , div [ class "lobby-listings flex row justify-start align-start" ]
                    [ div [ class "lobby-list flex column align-center" ] <|
                        append
                            [ div
                                [ class
                                    "lobby-list-header standard-header flex align-center justify-end"
                                ]
                                [ button
                                    [ class "submit-button"
                                    , classList [ ( "selected", isCreateLobby ) ]
                                    , onClick
                                        (if isCreateLobby then
                                            NavPush "/lobby"

                                         else
                                            NavPush "/lobby/create"
                                        )
                                    ]
                                    [ text "Create Lobby" ]
                                ]
                            ]
                            (buildLobbyList model)
                    , div [ class "lobby-list flex column align-center" ] <|
                        append
                            [ div
                                [ class
                                    "lobby-list-header standard-header flex align-center justify-end"
                                ]
                                [ text "Active Games" ]
                            ]
                            (buildGameList model)
                    ]
                ]
            ]
        )


isLobbyHidden : Maybe Route -> Bool
isLobbyHidden route =
    case route of
        Just Lobbies ->
            False

        Just LobbyCreate ->
            False

        Just (LobbyDetails _) ->
            False

        _ ->
            True


getLobbyHiddenClass : Maybe Route -> String
getLobbyHiddenClass route =
    if isLobbyHidden route then
        "-hidden standard-border"

    else
        " standard-border flex row"


buildLobbyList : Model -> List (Html Msg)
buildLobbyList model =
    let
        modelLobby =
            model.lobby
    in
    case model.player of
        Nothing ->
            [ div [ class "msg-text" ]
                [ text "No Lobbies Found" ]
            ]

        Just player ->
            case modelLobby.lobbies of
                Nothing ->
                    [ div [ class "msg-text" ]
                        [ text "Loading Lobbies..." ]
                    ]

                Just [] ->
                    [ div [ class "msg-text" ]
                        [ text "No Lobbies Found" ]
                    ]

                Just ls ->
                    let
                        sortedLobbies =
                            sortLobbies player.id ls

                        selectedLobbyId =
                            case model.route of
                                Just (LobbyDetails lobbyId) ->
                                    lobbyId

                                _ ->
                                    -1
                    in
                    [ ul [ class "lobby-entries flex column align-center justify-start" ]
                        (createLobbyEntries sortedLobbies player.id selectedLobbyId)
                    ]


sortLobbies : Int -> List Lobby -> List Lobby
sortLobbies playerId lobbies =
    let
        splitByPlayer =
            List.partition
                (\lb ->
                    listContainsPlayer playerId lb.teamA
                        || listContainsPlayer playerId lb.teamB
                )
                lobbies

        splitByCreator =
            List.partition
                (\lb -> lb.createdById == playerId)
                (Tuple.first splitByPlayer)
    in
    List.sortBy .createdDate (Tuple.first splitByCreator)
        ++ List.sortBy .createdDate (Tuple.second splitByCreator)
        ++ List.sortBy .createdDate (Tuple.second splitByPlayer)


createLobbyEntries : List Lobby -> Int -> Int -> List (Html Msg)
createLobbyEntries lobbies playerId selectedLobbyId =
    let
        h =
            head lobbies

        t =
            tail lobbies
    in
    case h of
        Nothing ->
            []

        Just lobby ->
            let
                isSelectedLobby =
                    lobby.id == selectedLobbyId

                isCreatorLobby =
                    lobby.createdById == playerId

                isMemberLobby =
                    (listContainsPlayer playerId lobby.teamA
                        || listContainsPlayer playerId lobby.teamB
                    )
                        && not isCreatorLobby

                playerCount =
                    List.length lobby.teamA + List.length lobby.teamB

                lobbyHtml =
                    [ li
                        [ class "lobby-entry" ]
                        [ button
                            [ classList
                                [ ( "selected", isSelectedLobby )
                                , ( "creator", isCreatorLobby )
                                , ( "member", isMemberLobby )
                                ]
                            , onClick
                                (if isSelectedLobby then
                                    NavPush "/lobby"

                                 else
                                    NavPush <| "/lobby/" ++ String.fromInt lobby.id
                                )
                            ]
                            [ div [ class "name" ]
                                [ text lobby.name ]
                            , div
                                [ class "form-row flex row align-center justify-center" ]
                                [ div [ class "descriptor" ]
                                    [ text (gameTypeToString lobby.gameType) ]
                                , div [ class "descriptor" ]
                                    [ text (gameFullString lobby.gameType playerCount) ]
                                ]
                            ]
                        ]
                    ]
            in
            case t of
                Nothing ->
                    lobbyHtml

                Just moreLobbies ->
                    lobbyHtml ++ createLobbyEntries moreLobbies playerId selectedLobbyId


buildGameList : Model -> List (Html Msg)
buildGameList model =
    case model.player of
        Nothing ->
            [ div [ class "msg-text" ]
                [ text "No Games Found" ]
            ]

        Just player ->
            let
                modelLobby =
                    model.lobby
            in
            case modelLobby.games of
                Nothing ->
                    [ div [ class "msg-text" ]
                        [ text "Loading Games..." ]
                    ]

                Just [] ->
                    [ div [ class "msg-text" ]
                        [ text "No Games Found" ]
                    ]

                Just gs ->
                    let
                        sortedGames =
                            sortGames player.id gs
                    in
                    [ ul [ class "lobby-entries flex column align-center justify-start" ]
                        (createGameEntries player.id sortedGames)
                    ]


sortGames : Int -> List GameMeta -> List GameMeta
sortGames playerId games =
    let
        splitByPlayer =
            List.partition
                (\g -> listContainsPlayer playerId g.players)
                games
    in
    Tuple.first splitByPlayer ++ Tuple.second splitByPlayer


createGameEntries : Int -> List GameMeta -> List (Html Msg)
createGameEntries playerId games =
    let
        h =
            head games

        t =
            tail games
    in
    case h of
        Nothing ->
            []

        Just game ->
            let
                isMemberGame =
                    listContainsPlayer playerId game.players

                gameHtml =
                    [ li [ class "lobby-entry" ]
                        [ button
                            [ classList [ ( "member", isMemberGame ) ]
                            , onClick
                                (NavPush <| "/game/" ++ String.fromInt game.id)
                            ]
                            [ div [ class "name" ]
                                [ text game.name ]
                            , div [ class "players flex row align-center justify-start" ]
                                (buildPlayerList game.players)
                            ]
                        ]
                    ]
            in
            case t of
                Nothing ->
                    gameHtml

                Just moreGames ->
                    gameHtml ++ createGameEntries playerId moreGames


listContainsPlayer : Int -> List Player -> Bool
listContainsPlayer playerId players =
    List.any (\player -> player.id == playerId) players


gameTypeToString : Int -> String
gameTypeToString gameType =
    case gameType of
        0 ->
            "2-player"

        1 ->
            "4-player"

        _ ->
            "Unknown"


gameFullString : Int -> Int -> String
gameFullString gameType playerCount =
    let
        maxPlayers =
            case gameType of
                0 ->
                    2

                1 ->
                    4

                _ ->
                    0
    in
    if playerCount < maxPlayers then
        "Open"

    else
        "Full"


buildPlayerList : List Player -> List (Html Msg)
buildPlayerList players =
    let
        head =
            List.head players

        tail =
            List.tail players
    in
    case head of
        Nothing ->
            []

        Just h ->
            let
                headEntry =
                    div [ class "player" ] [ text h.name ]
            in
            case tail of
                Nothing ->
                    [ headEntry ]

                Just [] ->
                    [ headEntry ]

                Just t ->
                    headEntry :: buildPlayerList t
