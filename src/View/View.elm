module View.View exposing (..)

import Browser
import Model.Model exposing (..)
import Routing exposing (..)
import View.Game as VGame
import View.Loading as VLoading
import View.Lobby as VLobby
import View.LobbyCreate as VLobbyCreate
import View.LobbyDetails as VLobbyDetails
import View.Login as VLogin


view : Model -> Browser.Document Msg
view model =
    case model.route of
        Just Lobbies ->
            contentView model

        Just LobbyCreate ->
            contentView model

        Just (LobbyDetails _) ->
            contentView model

        Just (CurrentGame _) ->
            contentView model

        Just Login ->
            VLogin.view model

        Just Loading ->
            VLoading.view model

        Nothing ->
            contentView model


contentView : Model -> Browser.Document Msg
contentView model =
    { title = "BattleBox"
    , body =
        [ VLobby.view model
        , VLobbyCreate.view model
        , VLobbyDetails.view model
        , VGame.view model
        ]
    }
