module Routing exposing (..)

import Url.Parser exposing ((</>), Parser, int, map, oneOf, s)


type Route
    = Login
    | Lobbies
    | LobbyCreate
    | LobbyDetails Int
    | CurrentGame Int
    | Loading


route : Parser (Route -> a) a
route =
    oneOf
        [ map Login (s "login")
        , map Lobbies (s "lobby")
        , map LobbyCreate (s "lobby" </> s "create")
        , map LobbyDetails (s "lobby" </> int)
        , map CurrentGame (s "game" </> int)
        ]
