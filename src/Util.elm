module Util exposing (doesDictEntryEqual, getMaybeInt, getMaybeString)

import Dict


doesDictEntryEqual : Dict.Dict String String -> String -> String -> Bool
doesDictEntryEqual dict key value =
    let
        dictValue =
            Dict.get key dict
    in
    case dictValue of
        Nothing ->
            False

        Just dictVal ->
            dictVal == value


getMaybeString : Maybe String -> String -> String
getMaybeString string default =
    case string of
        Just str ->
            str

        Nothing ->
            default


getMaybeInt : Maybe Int -> Int -> Int
getMaybeInt int default =
    case int of
        Just i ->
            i

        Nothing ->
            default
