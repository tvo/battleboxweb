module ApiCalls exposing (..)

import Http
import Json.Decode as JsonD
import Json.Encode as JsonE
import Model.Common exposing (GameMeta, Player)
import Model.Lobby exposing (Lobby)
import Model.Model exposing (..)


createRequestHeader : String -> List Http.Header
createRequestHeader playerId =
    [ Http.header "Content-Type" "application/json"
    , Http.header "playerId" playerId
    ]


createLobby : String -> Lobby -> Cmd Msg
createLobby playerId lobby =
    Http.request
        { method = "Post"
        , headers = createRequestHeader playerId
        , url = "/api/lobby"
        , body = Http.jsonBody (lobbyEncode lobby)
        , expect = Http.expectJson LobbyCreateResult lobbyDecode
        , timeout = Just 5000
        , tracker = Nothing
        }


joinLobby : String -> Int -> String -> Cmd Msg
joinLobby playerId lobbyId team =
    Http.request
        { method = "Post"
        , headers = createRequestHeader playerId
        , url =
            "/api/lobby/"
                ++ String.fromInt lobbyId
                ++ "/join/"
                ++ team
        , body = Http.emptyBody
        , expect = Http.expectJson LobbyJoinResult lobbyDecode
        , timeout = Just 5000
        , tracker = Nothing
        }


leaveLobby : String -> Int -> Cmd Msg
leaveLobby playerId lobbyId =
    Http.request
        { method = "Post"
        , headers = createRequestHeader playerId
        , url =
            "/api/lobby/"
                ++ String.fromInt lobbyId
                ++ "/leave"
        , body = Http.emptyBody
        , expect = Http.expectJson LobbyJoinResult lobbyDecode
        , timeout = Just 5000
        , tracker = Nothing
        }


deleteLobby : String -> Int -> Cmd Msg
deleteLobby playerId lobbyId =
    Http.request
        { method = "Delete"
        , headers = createRequestHeader playerId
        , url = "/api/lobby/" ++ String.fromInt lobbyId
        , body = Http.emptyBody
        , expect = Http.expectJson LobbyDeleteResult JsonD.int
        , timeout = Just 5000
        , tracker = Nothing
        }


lobbyToGame : String -> Int -> Cmd Msg
lobbyToGame playerId lobbyId =
    Http.request
        { method = "Post"
        , headers = createRequestHeader playerId
        , url = "/api/game"
        , body = Http.jsonBody (lobbyIdEncode lobbyId)
        , expect = Http.expectJson GameCreateResult gameDecode
        , timeout = Just 5000
        , tracker = Nothing
        }


getPlayer : String -> Cmd Msg
getPlayer playerId =
    Http.request
        { method = "Get"
        , headers = createRequestHeader playerId
        , url = "/api/player/me"
        , body = Http.emptyBody
        , expect = Http.expectJson LookupPlayerResult playerDecode
        , timeout = Just 5000
        , tracker = Nothing
        }


getLobbies : String -> Cmd Msg
getLobbies playerId =
    Http.request
        { method = "Get"
        , headers = createRequestHeader playerId
        , url = "/api/lobby"
        , body = Http.emptyBody
        , expect = Http.expectJson LookupLobbiesResult <| JsonD.list lobbyDecode
        , timeout = Just 5000
        , tracker = Nothing
        }


getGames : String -> Cmd Msg
getGames playerId =
    Http.request
        { method = "Get"
        , headers = createRequestHeader playerId
        , url = "/api/game"
        , body = Http.emptyBody
        , expect = Http.expectJson LookupGamesResult <| JsonD.list gameDecode
        , timeout = Just 5000
        , tracker = Nothing
        }


getGame : String -> Int -> Cmd Msg
getGame playerId gameId =
    Http.request
        { method = "Get"
        , headers = createRequestHeader playerId
        , url = "/api/game/" ++ String.fromInt gameId
        , body = Http.emptyBody
        , expect = Http.expectJson LookupGameResult gameDecode
        , timeout = Just 5000
        , tracker = Nothing
        }


lobbyEncode : Lobby -> JsonE.Value
lobbyEncode lobby =
    JsonE.object
        [ ( "name", JsonE.string lobby.name )
        , ( "gameType", JsonE.int lobby.gameType )
        , ( "teamA", JsonE.list JsonE.int <| getPlayerIds lobby.teamA )
        , ( "teamB", JsonE.list JsonE.int <| getPlayerIds lobby.teamB )
        ]


lobbyIdEncode : Int -> JsonE.Value
lobbyIdEncode lobbyId =
    JsonE.object
        [ ( "lobbyId", JsonE.int lobbyId ) ]


getPlayerIds : List Player -> List Int
getPlayerIds players =
    List.map (\p -> p.id) players


playerDecode : JsonD.Decoder Player
playerDecode =
    JsonD.map2 Player
        (JsonD.field "id" JsonD.int)
        (JsonD.field "name" JsonD.string)


lobbyDecode : JsonD.Decoder Lobby
lobbyDecode =
    JsonD.map7 Lobby
        (JsonD.field "id" JsonD.int)
        (JsonD.field "name" JsonD.string)
        (JsonD.field "createdById" JsonD.int)
        (JsonD.field "createdDate" JsonD.string)
        (JsonD.field "gameType" JsonD.int)
        (JsonD.field "teamA" <| JsonD.list playerDecode)
        (JsonD.field "teamB" <| JsonD.list playerDecode)


gameDecode : JsonD.Decoder GameMeta
gameDecode =
    JsonD.map3 GameMeta
        (JsonD.field "id" JsonD.int)
        (JsonD.field "name" JsonD.string)
        (JsonD.field "players" <| JsonD.list playerDecode)
