module Update.Lobby exposing (update)

import ApiCalls as ApiC
import Browser.Navigation exposing (pushUrl)
import Dict
import Http
import Model.Common exposing (GameMeta, Player)
import Model.Lobby exposing (..)
import Model.Model exposing (..)
import Util exposing (getMaybeInt, getMaybeString)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NewLobbyUpdate field value ->
            ( updateFieldValue model field value, Cmd.none )

        CreateLobby ->
            ( model, createLobby model )

        JoinLobby lobbyId team ->
            case model.playerId of
                Nothing ->
                    ( model, Cmd.none )

                Just playerId ->
                    ( model, ApiC.joinLobby playerId lobbyId team )

        LeaveLobby lobbyId ->
            case model.playerId of
                Nothing ->
                    ( model, Cmd.none )

                Just playerId ->
                    ( model, ApiC.leaveLobby playerId lobbyId )

        DeleteLobby lobbyId ->
            case model.playerId of
                Nothing ->
                    ( model, Cmd.none )

                Just playerId ->
                    ( model, ApiC.deleteLobby playerId lobbyId )

        LobbyToGame lobbyId ->
            case model.playerId of
                Nothing ->
                    ( model, Cmd.none )

                Just playerId ->
                    ( removeLobby lobbyId model
                    , Cmd.batch
                        [ ApiC.lobbyToGame playerId lobbyId
                        , pushUrl model.navKey "/lobby"
                        ]
                    )

        LobbyCreateResult result ->
            handleLobbyCreateReturn result model

        LobbyJoinResult result ->
            handleLobbyUpdateReturn result model

        LobbyLeaveResult result ->
            handleLobbyUpdateReturn result model

        LobbyDeleteResult result ->
            handleLobbyDeleteReturn result model

        GameCreateResult result ->
            handleGameCreate result model

        LookupLobbiesResult result ->
            handleLobbiesReturn result model

        LookupGamesResult result ->
            handleGamesReturn result model

        _ ->
            ( model, Cmd.none )


updateFieldValue : Model -> String -> String -> Model
updateFieldValue model field value =
    let
        lobby =
            model.lobby

        lobbyUpdate =
            { lobby
                | newLobbyForm =
                    Dict.update
                        field
                        (Maybe.map (\_ -> value))
                        lobby.newLobbyForm
            }
    in
    { model | lobby = lobbyUpdate }


createLobby : Model -> Cmd Msg
createLobby model =
    let
        lobby =
            model.lobby
    in
    case model.playerId of
        Nothing ->
            Cmd.none

        Just playerId ->
            case model.player of
                Nothing ->
                    Cmd.none

                Just player ->
                    ApiC.createLobby playerId <|
                        lobbyFromNewLobbyForm player lobby.newLobbyForm


lobbyFromNewLobbyForm : Player -> Dict.Dict String String -> Lobby
lobbyFromNewLobbyForm player form =
    let
        name =
            getMaybeString (Dict.get "name" form) ""

        gameType =
            getMaybeInt
                (String.toInt <|
                    getMaybeString (Dict.get "gameType" form) "0"
                )
                0

        team =
            getMaybeString (Dict.get "team" form) ""
    in
    if team == "A" then
        Lobby -1 name -1 "" gameType [ player ] []

    else
        Lobby -1 name -1 "" gameType [] [ player ]


handleLobbyCreateReturn : Result Http.Error Lobby -> Model -> ( Model, Cmd Msg )
handleLobbyCreateReturn result model =
    let
        navPush =
            pushUrl model.navKey "/lobby"

        currentLobby =
            model.lobby
    in
    case result of
        Ok lobby ->
            case currentLobby.lobbies of
                Just lobbies ->
                    let
                        lobbyUpdate =
                            { currentLobby
                                | lobbies =
                                    Just <| lobby :: lobbies
                            }
                    in
                    ( { model | lobby = lobbyUpdate }, navPush )

                Nothing ->
                    let
                        lobbyUpdate =
                            { currentLobby | lobbies = Just [ lobby ] }
                    in
                    ( { model | lobby = lobbyUpdate }, navPush )

        Err _ ->
            ( model, Cmd.none )


handleLobbyUpdateReturn : Result Http.Error Lobby -> Model -> ( Model, Cmd Msg )
handleLobbyUpdateReturn result model =
    case result of
        Ok lobby ->
            let
                currentLobby =
                    model.lobby

                currentLobbies =
                    case currentLobby.lobbies of
                        Just lobbies ->
                            lobbies

                        Nothing ->
                            []

                newLobbies =
                    currentLobbies
                        |> List.map
                            (\lby ->
                                if lby.id == lobby.id then
                                    lobby

                                else
                                    lby
                            )
                        |> Just
            in
            ( { model | lobby = { currentLobby | lobbies = newLobbies } }
            , Cmd.none
            )

        Err _ ->
            ( model, Cmd.none )


handleLobbyDeleteReturn : Result Http.Error Int -> Model -> ( Model, Cmd Msg )
handleLobbyDeleteReturn result model =
    case result of
        Ok id ->
            let
                updateModel =
                    removeLobby id model
            in
            ( updateModel, Cmd.none )

        Err _ ->
            ( model, Cmd.none )


handleLobbiesReturn : Result Http.Error (List Lobby) -> Model -> ( Model, Cmd Msg )
handleLobbiesReturn result model =
    let
        currentLobby =
            model.lobby
    in
    case result of
        Ok lobbies ->
            ( { model | lobby = { currentLobby | lobbies = Just lobbies } }
            , Cmd.none
            )

        Err _ ->
            ( { model | lobby = { currentLobby | lobbies = Just [] } }
            , Cmd.none
            )


handleGamesReturn : Result Http.Error (List GameMeta) -> Model -> ( Model, Cmd Msg )
handleGamesReturn result model =
    let
        currentLobby =
            model.lobby
    in
    case result of
        Ok games ->
            ( { model | lobby = { currentLobby | games = Just games } }
            , Cmd.none
            )

        Err _ ->
            ( { model | lobby = { currentLobby | games = Just [] } }
            , Cmd.none
            )


handleGameCreate : Result Http.Error GameMeta -> Model -> ( Model, Cmd Msg )
handleGameCreate result model =
    let
        currentLobby =
            model.lobby

        currentGames =
            case currentLobby.games of
                Just games ->
                    games

                Nothing ->
                    []
    in
    case result of
        Ok game ->
            ( { model
                | lobby =
                    { currentLobby
                        | games = Just (currentGames ++ [ game ])
                    }
              }
            , Cmd.none
            )

        Err _ ->
            ( model, pushUrl model.navKey "/lobby" )


removeLobby : Int -> Model -> Model
removeLobby lobbyId model =
    let
        currentLobby =
            model.lobby

        currentLobbies =
            case currentLobby.lobbies of
                Just lobbies ->
                    lobbies

                Nothing ->
                    []
    in
    { model
        | lobby =
            { currentLobby
                | lobbies =
                    Just
                        (List.partition
                            (\lb -> lb.id /= lobbyId)
                            currentLobbies
                            |> Tuple.first
                        )
            }
    }
