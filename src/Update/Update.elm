module Update.Update exposing (update)

import Model.Common exposing (ScreenSize)
import Model.Model exposing (..)
import Update.Game as UGame
import Update.Lobby as ULobby
import Update.Nav as UNav


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LoginIdUpdate id ->
            ( { model | playerId = Just id }, Cmd.none )

        ScreenSizeUpdate width height ->
            ( { model | screenSize = ScreenSize width height }, Cmd.none )

        NoOp ->
            ( model, Cmd.none )

        NewLobbyUpdate _ _ ->
            ULobby.update msg model

        CreateLobby ->
            ULobby.update msg model

        JoinLobby _ _ ->
            ULobby.update msg model

        LeaveLobby _ ->
            ULobby.update msg model

        DeleteLobby _ ->
            ULobby.update msg model

        LobbyToGame _ ->
            ULobby.update msg model

        LobbyCreateResult _ ->
            ULobby.update msg model

        LobbyJoinResult _ ->
            ULobby.update msg model

        LobbyLeaveResult _ ->
            ULobby.update msg model

        LobbyDeleteResult _ ->
            ULobby.update msg model

        GameCreateResult _ ->
            ULobby.update msg model

        LookupLobbiesResult _ ->
            ULobby.update msg model

        LookupGamesResult _ ->
            ULobby.update msg model

        LookupGameResult _ ->
            UGame.update msg model

        MouseMove _ ->
            UGame.update msg model

        CoinMouseDown _ _ ->
            UGame.update msg model

        CoinMouseUp ->
            UGame.update msg model

        ZoneMouseEnter _ ->
            UGame.update msg model

        ZoneMouseExit ->
            UGame.update msg model

        IdEntered ->
            UNav.update msg model

        Logout ->
            UNav.update msg model

        LookupPlayerResult _ ->
            UNav.update msg model

        NavPush _ ->
            UNav.update msg model

        UrlRequested _ ->
            UNav.update msg model

        UrlChanged _ ->
            UNav.update msg model
