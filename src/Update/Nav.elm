port module Update.Nav exposing (update)

import ApiCalls as ApiC
import Browser
import Browser.Navigation as Nav
import Http
import Json.Encode as JsonE
import Model.Common exposing (Player)
import Model.Lobby exposing (initialNewLobby)
import Model.Model exposing (..)
import Routing exposing (..)
import Update.Extra exposing (andThen)
import Url
import Url.Parser as UrlParser
import Util exposing (getMaybeString)


port cache : JsonE.Value -> Cmd msg


port clearCache : () -> Cmd msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        IdEntered ->
            handleIdEntered model

        Logout ->
            handleLogout model

        LookupPlayerResult result ->
            handlePlayerReturn result model

        NavPush path ->
            ( model, Nav.pushUrl model.navKey path )

        UrlRequested urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.navKey <| Url.toString url )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            handleInternalUrl url model

        _ ->
            ( model, Cmd.none )


handleIdEntered : Model -> ( Model, Cmd Msg )
handleIdEntered model =
    case model.playerId of
        Nothing ->
            ( model, Nav.pushUrl model.navKey "/login" )

        Just playerId ->
            ( model
            , Cmd.batch
                [ cache <| JsonE.string playerId
                , ApiC.getPlayer playerId
                ]
            )


handleLogout : Model -> ( Model, Cmd Msg )
handleLogout model =
    ( { model | playerId = Nothing, player = Nothing }
    , Cmd.batch [ clearCache (), Nav.pushUrl model.navKey "/lobby" ]
    )


handlePlayerReturn : Result Http.Error Player -> Model -> ( Model, Cmd Msg )
handlePlayerReturn result model =
    case result of
        Ok player ->
            case model.targetUrl of
                Nothing ->
                    ( { model | player = Just player }
                    , Nav.pushUrl model.navKey "/lobby"
                    )

                Just targetUrl ->
                    ( { model | player = Just player }
                    , Nav.pushUrl model.navKey <| Url.toString targetUrl
                    )

        Err _ ->
            ( { model | playerId = Nothing }
            , Cmd.batch [ clearCache (), Nav.pushUrl model.navKey "/login" ]
            )


handleInternalUrl : Url.Url -> Model -> ( Model, Cmd Msg )
handleInternalUrl url model =
    let
        urlRoute =
            UrlParser.parse route url

        targetUrl =
            case model.targetUrl of
                Nothing ->
                    if
                        urlRoute
                            == Just Login
                            || urlRoute
                            == Nothing
                    then
                        { url | path = "/lobby" }

                    else
                        url

                Just tUrl ->
                    tUrl
    in
    if model.playerId == Nothing then
        if urlRoute /= Just Login then
            ( { model
                | targetUrl = Just targetUrl
              }
            , Nav.pushUrl model.navKey "/login"
            )

        else
            ( { model | route = urlRoute, targetUrl = Just targetUrl }, Cmd.none )

    else if model.player == Nothing then
        ( { model | route = Just Loading, targetUrl = Just targetUrl }, Cmd.none )
            |> andThen update IdEntered

    else
        handleUpdatesForRoute model <|
            UrlParser.parse route targetUrl


handleUpdatesForRoute : Model -> Maybe Route -> ( Model, Cmd Msg )
handleUpdatesForRoute model route =
    let
        newModel =
            { model | route = route, targetUrl = Nothing, currentGame = Nothing }
    in
    case route of
        Just Lobbies ->
            ( newModel
            , Cmd.batch
                [ ApiC.getLobbies <| getMaybeString model.playerId ""
                , ApiC.getGames <| getMaybeString model.playerId ""
                ]
            )

        Just LobbyCreate ->
            let
                modelLobby =
                    model.lobby
            in
            ( { newModel | lobby = { modelLobby | newLobbyForm = initialNewLobby } }
            , getLobbiesIfNotLoaded newModel
            )

        Just (LobbyDetails _) ->
            ( newModel, getLobbiesIfNotLoaded newModel )

        Just (CurrentGame gameId) ->
            ( newModel, ApiC.getGame (getMaybeString model.playerId "") gameId )

        _ ->
            ( newModel, Cmd.none )


getLobbiesIfNotLoaded : Model -> Cmd Msg
getLobbiesIfNotLoaded model =
    let
        modelLobby =
            model.lobby
    in
    if modelLobby.lobbies == Nothing || modelLobby.games == Nothing then
        Cmd.batch
            [ ApiC.getLobbies <| getMaybeString model.playerId ""
            , ApiC.getGames <| getMaybeString model.playerId ""
            ]

    else
        Cmd.none
