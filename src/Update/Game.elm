module Update.Game exposing (update)

import Http
import Model.Common exposing (GameMeta, SvgCoord)
import Model.Game exposing (..)
import Model.Model exposing (..)
import Model.Util exposing (coinLocationsEqual)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LookupGameResult result ->
            updateCurrentGameState result model

        MouseMove mouseCoord ->
            ( updateMouseCoord mouseCoord model, Cmd.none )

        CoinMouseDown coinLoc coinCoord ->
            ( startCoinDrag coinLoc coinCoord model, Cmd.none )

        CoinMouseUp ->
            ( updateCoinZone model, Cmd.none )

        ZoneMouseEnter zone ->
            ( updateHoverZone zone model, Cmd.none )

        ZoneMouseExit ->
            ( updateHoverZone NoZone model, Cmd.none )

        _ ->
            ( model, Cmd.none )


updateCurrentGameState : Result Http.Error GameMeta -> Model -> ( Model, Cmd Msg )
updateCurrentGameState result model =
    case result of
        Ok game ->
            ( { model
                | currentGame =
                    Just
                        (Game Nothing
                            NoZone
                            game
                            (SvgCoord 0 0)
                            boardCoords2p
                            controlCoords2p
                            (State Battle <|
                                [ CoinLocation 20 2 (Hand 1)
                                , CoinLocation 18 1 (Hand 1)
                                ]
                            )
                        )
              }
            , Cmd.none
            )

        Err _ ->
            ( { model | currentGame = Nothing }, Cmd.none )


updateMouseCoord : SvgCoord -> Model -> Model
updateMouseCoord newCoord model =
    case model.currentGame of
        Nothing ->
            model

        Just game ->
            { model | currentGame = Just { game | mouseCoord = newCoord } }


startCoinDrag : CoinLocation -> SvgCoord -> Model -> Model
startCoinDrag coinLoc coinCoord model =
    let
        currentGame =
            model.currentGame

        coinDragLoc =
            CoinLocation coinLoc.coinType 1 coinLoc.zone
    in
    case currentGame of
        Nothing ->
            model

        Just game ->
            let
                updateGame =
                    removeCoin coinDragLoc game
            in
            { model
                | currentGame =
                    Just
                        { updateGame
                            | coinDrag =
                                Just <|
                                    CoinDrag coinDragLoc coinCoord game.mouseCoord
                        }
            }


updateHoverZone : Zone -> Model -> Model
updateHoverZone zone model =
    let
        currentGame =
            model.currentGame
    in
    case currentGame of
        Nothing ->
            model

        Just game ->
            { model | currentGame = Just { game | hoverZone = zone } }


updateCoinZone : Model -> Model
updateCoinZone model =
    let
        currentGame =
            model.currentGame
    in
    case currentGame of
        Nothing ->
            model

        Just game ->
            let
                gameUpdate =
                    { game | coinDrag = Nothing, hoverZone = NoZone }

                targetZone =
                    game.hoverZone
            in
            case game.coinDrag of
                Nothing ->
                    { model | currentGame = Just gameUpdate }

                Just coinDrag ->
                    case targetZone of
                        NoZone ->
                            { model
                                | currentGame =
                                    Just <|
                                        addCoin coinDrag.coinLoc gameUpdate
                            }

                        _ ->
                            let
                                originCoinLoc =
                                    coinDrag.coinLoc

                                moveCoinLoc =
                                    CoinLocation
                                        originCoinLoc.coinType
                                        originCoinLoc.quantity
                                        targetZone
                            in
                            { model
                                | currentGame =
                                    Just <|
                                        addCoin moveCoinLoc gameUpdate
                            }


removeCoin : CoinLocation -> Game -> Game
removeCoin coinLoc game =
    let
        gameState =
            game.state

        toRemoveAndOthers =
            getCoinLocAndOthers coinLoc gameState.coins

        toRemove =
            Tuple.first toRemoveAndOthers

        otherCoins =
            Tuple.second toRemoveAndOthers
    in
    case toRemove of
        Nothing ->
            game

        Just removeCoinLoc ->
            let
                coinLocsToAppend =
                    if removeCoinLoc.quantity > coinLoc.quantity then
                        [ CoinLocation coinLoc.coinType
                            (removeCoinLoc.quantity - coinLoc.quantity)
                            coinLoc.zone
                        ]

                    else
                        []

                finalCoins =
                    otherCoins ++ coinLocsToAppend
            in
            { game
                | state = { gameState | coins = finalCoins }
            }


addCoin : CoinLocation -> Game -> Game
addCoin coinLoc game =
    let
        gameState =
            game.state

        toAddAndOthers =
            getCoinLocAndOthers coinLoc gameState.coins

        toAddTarget =
            Tuple.first toAddAndOthers

        otherCoins =
            Tuple.second toAddAndOthers

        coinLocQuantity =
            case toAddTarget of
                Nothing ->
                    coinLoc.quantity

                Just target ->
                    target.quantity + coinLoc.quantity

        finalCoins =
            otherCoins
                ++ [ CoinLocation coinLoc.coinType coinLocQuantity coinLoc.zone ]
    in
    { game
        | state = { gameState | coins = finalCoins }
    }


getCoinLocAndOthers : CoinLocation -> List CoinLocation -> ( Maybe CoinLocation, List CoinLocation )
getCoinLocAndOthers coinLoc coins =
    let
        coinLocAndOthers =
            List.partition
                (\cl -> coinLocationsEqual cl coinLoc)
                coins

        currentCoinLoc =
            List.head <| Tuple.first coinLocAndOthers
    in
    ( currentCoinLoc, Tuple.second coinLocAndOthers )
