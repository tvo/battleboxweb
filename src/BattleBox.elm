module BattleBox exposing (subscriptions)

import Browser
import Browser.Events exposing (onResize)
import Model.Model exposing (..)
import Update.Update exposing (update)
import View.View exposing (view)


main : Program String Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = UrlRequested
        , onUrlChange = UrlChanged
        }


subscriptions : Model -> Sub Msg
subscriptions _ =
    onResize ScreenSizeUpdate
