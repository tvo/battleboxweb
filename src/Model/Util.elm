module Model.Util exposing (coinLocationsEqual)

import Model.Game exposing (CoinLocation)


coinLocationsEqual : CoinLocation -> CoinLocation -> Bool
coinLocationsEqual coinLocA coinLocB =
    coinLocA.coinType
        == coinLocB.coinType
        && coinLocA.zone
        == coinLocB.zone
