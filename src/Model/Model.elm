module Model.Model exposing (..)

import Browser
import Browser.Dom exposing (getViewport)
import Browser.Navigation as Nav
import Http
import Json.Decode as JsonD
import Model.Common exposing (..)
import Model.Game exposing (..)
import Model.Lobby exposing (..)
import Routing exposing (..)
import Task
import Url


type Msg
    = LoginIdUpdate String
    | IdEntered
    | Logout
    | NewLobbyUpdate String String
    | CreateLobby
    | JoinLobby Int String
    | LeaveLobby Int
    | DeleteLobby Int
    | LobbyToGame Int
    | LobbyCreateResult (Result Http.Error Lobby)
    | LobbyJoinResult (Result Http.Error Lobby)
    | LobbyLeaveResult (Result Http.Error Lobby)
    | LobbyDeleteResult (Result Http.Error Int)
    | GameCreateResult (Result Http.Error GameMeta)
    | LookupPlayerResult (Result Http.Error Player)
    | LookupLobbiesResult (Result Http.Error (List Lobby))
    | LookupGamesResult (Result Http.Error (List GameMeta))
    | LookupGameResult (Result Http.Error GameMeta)
    | CoinMouseDown CoinLocation SvgCoord
    | CoinMouseUp
    | ZoneMouseEnter Zone
    | ZoneMouseExit
    | ScreenSizeUpdate Int Int
    | MouseMove SvgCoord
    | NavPush String
    | UrlRequested Browser.UrlRequest
    | UrlChanged Url.Url
    | NoOp


type alias Model =
    { playerId : Maybe String
    , player : Maybe Player
    , navKey : Nav.Key
    , route : Maybe Route
    , targetUrl : Maybe Url.Url
    , screenSize : ScreenSize
    , lobby : LobbyModel
    , currentGame : Maybe Game
    }


init : String -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        playerId =
            case JsonD.decodeString JsonD.string flags of
                Ok id ->
                    Just id

                _ ->
                    Nothing

        model =
            initialModel playerId key
    in
    ( model
    , Cmd.batch
        [ Nav.pushUrl model.navKey <| Url.toString url
        , Task.perform
            (\{ viewport } ->
                ScreenSizeUpdate (round viewport.width) (round viewport.height)
            )
            getViewport
        ]
    )


initialModel : Maybe String -> Nav.Key -> Model
initialModel playerId navKey =
    { playerId = playerId
    , player = Nothing
    , navKey = navKey
    , route = Just Loading
    , targetUrl = Nothing
    , screenSize = ScreenSize 0 0
    , lobby = initialLobby
    , currentGame = Nothing
    }
