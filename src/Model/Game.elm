module Model.Game exposing (..)

import Model.Common exposing (GameMeta, SvgCoord)


type Mode
    = Draft
    | Battle


type Zone
    = NoZone
    | Draftable
    | Drafted Int
    | Board Int Int
    | Bag Int
    | Hand Int
    | DiscardUp Int
    | DiscardDown Int
    | Exile Int


type alias Game =
    { coinDrag : Maybe CoinDrag
    , hoverZone : Zone
    , meta : GameMeta
    , mouseCoord : SvgCoord
    , boardCoords : List AxialCoord
    , controlCoords : List AxialCoord
    , state : State
    }


type alias State =
    { gameMode : Mode
    , coins : List CoinLocation
    }


type alias CoinDrag =
    { coinLoc : CoinLocation
    , startCoinCoord : SvgCoord
    , startMouseCoord : SvgCoord
    }


type alias CoinLocation =
    { coinType : Int
    , quantity : Int
    , zone : Zone
    }


type alias AxialCoord =
    { q : Int
    , r : Int
    }


boardCoords2p : List AxialCoord
boardCoords2p =
    [ AxialCoord 0 0
    , AxialCoord 0 1
    , AxialCoord 1 0
    , AxialCoord 0 -1
    , AxialCoord -1 0
    , AxialCoord 0 2
    , AxialCoord 1 1
    , AxialCoord 2 -1
    , AxialCoord 2 -2
    , AxialCoord 1 -2
    , AxialCoord 0 -2
    , AxialCoord -1 -1
    , AxialCoord -2 1
    , AxialCoord -2 2
    , AxialCoord -1 2
    , AxialCoord 0 3
    , AxialCoord 2 1
    , AxialCoord 3 0
    , AxialCoord 3 -1
    , AxialCoord 3 -3
    , AxialCoord 1 -3
    , AxialCoord 0 -3
    , AxialCoord -2 -1
    , AxialCoord -3 0
    , AxialCoord -3 1
    , AxialCoord -3 3
    , AxialCoord -1 3
    ]


controlCoords2p : List AxialCoord
controlCoords2p =
    [ AxialCoord -1 1
    , AxialCoord 1 -1
    , AxialCoord 2 0
    , AxialCoord -2 0
    , AxialCoord 1 2
    , AxialCoord 3 -2
    , AxialCoord 2 -3
    , AxialCoord -1 -2
    , AxialCoord -3 2
    , AxialCoord -2 3
    ]


boardCoords4p : List AxialCoord
boardCoords4p =
    [ AxialCoord 4 -2
    , AxialCoord 4 -3
    , AxialCoord 5 -2
    , AxialCoord -4 2
    , AxialCoord -4 3
    , AxialCoord -5 2
    ]


controlCoords4p : List AxialCoord
controlCoords4p =
    [ AxialCoord 4 -1
    , AxialCoord 5 -3
    , AxialCoord -4 1
    , AxialCoord -5 3
    ]
