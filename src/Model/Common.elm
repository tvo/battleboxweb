module Model.Common exposing (..)


type alias GameMeta =
    { id : Int
    , name : String
    , players : List Player
    }


type alias SvgCoord =
    { x : Float
    , y : Float
    }


type alias Player =
    { id : Int
    , name : String
    }


type alias ScreenSize =
    { width : Int
    , height : Int
    }
