module Model.Lobby exposing (..)

import Dict
import Model.Common exposing (GameMeta, Player)


type alias LobbyModel =
    { lobbies : Maybe (List Lobby)
    , games : Maybe (List GameMeta)
    , newLobbyForm : Dict.Dict String String
    }


type alias Lobby =
    { id : Int
    , name : String
    , createdById : Int
    , createdDate : String
    , gameType : Int
    , teamA : List Player
    , teamB : List Player
    }


initialLobby : LobbyModel
initialLobby =
    { lobbies = Nothing
    , games = Nothing
    , newLobbyForm = initialNewLobby
    }


initialNewLobby : Dict.Dict String String
initialNewLobby =
    Dict.fromList
        [ ( "name", "" ), ( "gameType", "0" ), ( "team", "0" ) ]
