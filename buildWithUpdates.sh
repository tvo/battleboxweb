#!/bin/bash
docker stop battleboxweb
docker rm battleboxweb
docker pull registry.gitlab.com/tvo/battleboxweb
docker run --name battleboxweb -p 5000:80 -d registry.gitlab.com/tvo/battleboxweb

echo starting watchtower
echo make sure the path is correct
docker run -d -v /var/run/docker.sock:/var/run/docker.sock -v /home/trevor/.docker/config.json:/config.json containrrr/watchtower battleboxweb
