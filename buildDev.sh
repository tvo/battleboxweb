#!/bin/bash

docker stop battleboxweb
docker rm battleboxweb
docker build -t battleboxweb .
echo running container. ctrl c to exit.
docker run -it -p 8000:80 battleboxweb
