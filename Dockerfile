ARG BASE_ARCH=nginx

FROM codesimple/elm:0.19
WORKDIR /app

# install dependencies
COPY elm.json ./
COPY index.html ./
COPY src src
COPY assets assets

# build
RUN elm make src/BattleBox.elm --optimize --output=elm.js

#ENTRYPOINT elm repl --port=3001

FROM ${BASE_ARCH}
EXPOSE 80/tcp
COPY --from=0 /app/elm.js   /var/www/elm.js
COPY index.html /var/www/index.html
COPY assets /var/www/assets
COPY nginx.conf /etc/nginx/conf.d/default.conf
